/**
 *  Copyright (C) 2002-2015  The FreeCol Team
 *
 *  This file is part of FreeCol.
 *
 *  FreeCol is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  FreeCol is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FreeCol.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.sf.freecol.server.ai;

import net.sf.freecol.common.model.BuildableType;
import net.sf.freecol.common.model.Building;
import net.sf.freecol.common.model.BuildingType;
import net.sf.freecol.common.model.Colony;
import net.sf.freecol.common.model.ColonyTile;
import net.sf.freecol.common.model.Game;
import net.sf.freecol.common.model.GoodsContainer;
import net.sf.freecol.common.model.GoodsType;
import net.sf.freecol.common.model.Goods;
import net.sf.freecol.common.model.Location;
import net.sf.freecol.common.model.Map;
import net.sf.freecol.common.model.Tile;
import net.sf.freecol.common.model.TileType;
import net.sf.freecol.common.model.Unit;
import net.sf.freecol.common.model.UnitType;
import net.sf.freecol.common.util.LogBuilder;
import net.sf.freecol.server.ServerTestHelper;
import net.sf.freecol.server.model.ServerBuilding;
import net.sf.freecol.server.model.ServerPlayer;
import net.sf.freecol.server.model.ServerUnit;
import net.sf.freecol.util.test.FreeColTestCase;
import net.sf.freecol.util.test.FreeColTestUtils;
import net.sf.freecol.common.model.WorkLocation;
import net.sf.freecol.common.model.*;


public class AIGoodsTest extends FreeColTestCase {

	
    private static final UnitType farmer
    = spec().getUnitType("model.unit.expertFarmer");
    private static final UnitType veteranType
    = spec().getUnitType("model.unit.veteranSoldier");
	
    private static final UnitType artilleryType
    = spec().getUnitType("model.unit.artillery");
    private static final UnitType braveType
    = spec().getUnitType("model.unit.brave");
    
    private static final Role armedBraveRole
    = spec().getRole("model.role.armedBrave");
    private static final Role dragoonRole
    = spec().getRole("model.role.dragoon");
	
	private static final GoodsType foodType
    	= spec().getPrimaryFoodType();
	private static final GoodsType grainType
    	= spec().getGoodsType("model.goods.grain");
	private static final GoodsType hammersType
    	= spec().getGoodsType("model.goods.hammers");
	private static final GoodsType lumberType
    	= spec().getGoodsType("model.goods.lumber");
	private static final GoodsType oreType
    	= spec().getGoodsType("model.goods.ore");
	private static final GoodsType rumType
    	= spec().getGoodsType("model.goods.rum");
	private static final GoodsType sugarType
    	= spec().getGoodsType("model.goods.sugar");
	private static final GoodsType toolsType
    	= spec().getGoodsType("model.goods.tools");
	
    private static final TileType forestType
    	= spec().getTileType("model.tile.coniferForest");
    private static final TileType savannahType
    	= spec().getTileType("model.tile.savannah");
    private static final TileType mountainType
    	= spec().getTileType("model.tile.mountains");
    	
	
    private LogBuilder lb = new LogBuilder(0); // dummy


    @Override
    public void tearDown() throws Exception {
        ServerTestHelper.stopServerGame();
        super.tearDown();
    }
    
    public void testSetGoods() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
        
        Goods lettuce = new Goods(game, null, sugarType, 7);
        
        assertEquals(null, testGoods.getGoods());
        testGoods.setGoods(lettuce);
        assertEquals(lettuce, testGoods.getGoods());
        assertEquals(sugarType, testGoods.getGoodsType());
        assertEquals(7, testGoods.getGoodsAmount());
        testGoods.setGoodsAmount(0);
        assertEquals(0, testGoods.getGoodsAmount());
        
    }
    
    public void testCanMove() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
        assertEquals(false, testGoods.canMove());
    }
    
    public void testJoinTransport() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
        Direction direction = Direction.NE;
        assertEquals(false, testGoods.joinTransport(new Unit(game, "Soldier"), direction));
        

        //Setting up for second test
        Map map = game.getMap();
        Tile tile1 = map.getTile(2, 2);
        ServerPlayer dutch = (ServerPlayer) game.getPlayerByNationId("model.nation.dutch");
        EuropeanAIPlayer aiDutch = (EuropeanAIPlayer)aiMain.getAIPlayer(dutch);
        Unit soldier = new ServerUnit(game, tile1, dutch, veteranType);
        
        //Setting direction to null
        //Setting Goods and setting goodsAmount
        Colony colony = new Colony(game, "Rose");
        colony.setGoodsContainer(new GoodsContainer(game, "new Container"));
    	Goods lettuce = new Goods(game, colony, sugarType, 7);
    	testGoods.setGoods(lettuce);
    	testGoods.setGoodsAmount(17);
        direction = null;
        
        //There is an error here with getting a new owner
        soldier.setType(braveType);
        soldier.setRole(dragoonRole);
        
        assertEquals(true, testGoods.joinTransport(soldier, direction));
        
        assertEquals(false, testGoods.leaveTransport(null));
    }
    
    public void testCheckIntegrity() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        Colony colony = new Colony(game, "Rose");
        colony.setGoodsContainer(new GoodsContainer(game, "new Container"));

        AIGoods testGoods = new AIGoods(aiMain, colony, sugarType, 7, colony);

    	Goods lettuce = new Goods(game, colony , sugarType, 7);
    	testGoods.setGoods(lettuce);
    	testGoods.setGoodsAmount(17);
    	assertEquals(1, testGoods.checkIntegrity(false));
    	
    	testGoods.setUnitialized(true);
    	
    	Goods patty = new Goods(game, null, sugarType, 7);
    	testGoods.setGoods(patty);
    	assertEquals(-1, testGoods.checkIntegrity(false));

    	//For some reason unable to test where getType() == null
    	
//    	Goods burger = new Goods(game, colony, new GoodsType("yo", new Specification()), 7);
//    	testGoods.setGoods(burger);
//    	assertEquals(1, testGoods.checkIntegrity(true));

    	Goods cheese = new Goods(game, colony, sugarType, -5);
    	testGoods.setGoods(cheese);
    	assertEquals(-1, testGoods.checkIntegrity(false));
    }
    
    //Test doesn't completely work
    public void testCheckIntegrityNullGoods() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        Colony colony = new Colony(game, "Rose");
        colony.setGoodsContainer(new GoodsContainer(game, "new Container"));

        AIGoods testGoods = new AIGoods(aiMain, colony, sugarType, 7, colony);
        
    	testGoods.setUnitialized(true);

    	assertEquals(-1, testGoods.checkIntegrity(false));
    }
    
    
    
//    public void testNullAiCarrierJoinTransport() {
//        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
//        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
//        
//        AIGoods testGoods = new AIGoods(aiMain, "10");
//        Direction direction = null;
//        Unit soldier = new Unit(game, "hey");
//        
//    	Goods lettuce = new Goods(game, null, sugarType, 7);
//    	testGoods.setGoods(lettuce);
//    	testGoods.setGoodsAmount(17);
//    	
//    	soldier.setOwner(new Player(game, "john"));
//    	
//        //Setting type of soldier
//        soldier.setType(braveType);
//        soldier.setRole(dragoonRole);
//        
//    	assertEquals(false, testGoods.joinTransport(soldier, direction));
//    }
    
    public void testNullJoinTransport() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
        Direction direction = Direction.NE;
        assertEquals(false, testGoods.joinTransport(null, direction));
    }
    
    public void testLeaveTransport() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
        Goods lettuce = new Goods(game, null, sugarType, 7);
        testGoods.setGoods(lettuce);
        assertEquals(false, testGoods.leaveTransport());

        //Bug where direction != null
        Direction direction = Direction.W;
        assertEquals(false, testGoods.leaveTransport(direction));
        assertEquals(false, testGoods.leaveTransport(null));
        
        //Testing the other branches
        
    }
    
    public void testGetDeliveryPath() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
    	Unit carrier = new Unit(game, "Soldier");
    	Goods lettuce = new Goods(game, null, sugarType, 7);
    	testGoods.setGoods(lettuce);
    }
    
    public void testGetXMLTagName() {
        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
        
        AIGoods testGoods = new AIGoods(aiMain, "10");
        assertEquals("aiGoods", testGoods.getXMLTagName());
    }
    
    
    
//    public void testGetTransportSource() {
//        Game game = ServerTestHelper.startServerGame(getTestMap(savannahType));
//        AIMain aiMain = ServerTestHelper.getServer().getAIMain();
//        
//        AIGoods testGoods = new AIGoods(aiMain, "10");
//        assertEquals(null, testGoods.getTransportSource());
//        Colony newColony = new Colony(game, "France");
//        
//        Goods lettuce = new Goods(game, newColony, sugarType, 7);
//        testGoods.setGoods(lettuce);
//        
//        assertEquals(newColony, testGoods.getTransportSource());
//    }
    
}

/**
 *  Copyright (C) 2002-2015  The FreeCol Team
 *
 *  This file is part of FreeCol.
 *
 *  FreeCol is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  FreeCol is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FreeCol.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.sf.freecol.common.model;

import java.io.StringWriter;
import java.util.List;

import net.sf.freecol.common.io.FreeColXMLWriter;
import net.sf.freecol.common.model.Colony;
import net.sf.freecol.common.model.Monarch.MonarchAction;
import net.sf.freecol.common.util.RandomChoice;
import net.sf.freecol.util.test.FreeColTestCase;
import junit.framework.TestCase;
import net.sf.freecol.common.model.Game;
import net.sf.freecol.common.model.Player;
import net.sf.freecol.common.model.Monarch;
import net.sf.freecol.common.model.Monarch.Force;
import net.sf.freecol.server.ServerTestHelper;
import net.sf.freecol.util.test.FreeColTestCase;
import net.sf.freecol.common.model.Turn;
import java.util.Random;


public class MonarchTest extends FreeColTestCase {

    public void testSerialize() {
        Game game = getStandardGame();
        Player dutch = game.getPlayerByNationId("model.nation.dutch");

        try {
            StringWriter sw = new StringWriter();
            FreeColXMLWriter xw = new FreeColXMLWriter(sw,
                FreeColXMLWriter.WriteScope.toSave());

            dutch.getMonarch().toXML(xw);

            xw.close();

        } catch (Exception e) {
            fail(e.toString());
        }
    }

    public void testTaxActionChoices() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");

        // grace period has not yet expired
        List<RandomChoice<MonarchAction>> choices
            = dutch.getMonarch().getActionChoices();
        assertTrue(choices.isEmpty());

        Colony colony = getStandardColony();
        game.setTurn(new Turn(100));
        dutch.setTax(Monarch.MINIMUM_TAX_RATE / 2);
        choices = dutch.getMonarch().getActionChoices();
        assertTrue(choicesContain(choices, MonarchAction.RAISE_TAX_WAR));
        assertTrue(choicesContain(choices, MonarchAction.RAISE_TAX_ACT));
        assertFalse(choicesContain(choices, MonarchAction.LOWER_TAX_WAR));
        assertFalse(choicesContain(choices, MonarchAction.LOWER_TAX_OTHER));

        int maximumTax = spec().getInteger(GameOptions.MAXIMUM_TAX);
        dutch.setTax(maximumTax / 2);
        choices = dutch.getMonarch().getActionChoices();
        assertTrue(choicesContain(choices, MonarchAction.RAISE_TAX_WAR));
        assertTrue(choicesContain(choices, MonarchAction.RAISE_TAX_ACT));
        assertTrue(choicesContain(choices, MonarchAction.LOWER_TAX_WAR));
        assertTrue(choicesContain(choices, MonarchAction.LOWER_TAX_OTHER));

        dutch.setTax(maximumTax + 2);
        choices = dutch.getMonarch().getActionChoices();
        assertFalse(choicesContain(choices, MonarchAction.RAISE_TAX_WAR));
        assertFalse(choicesContain(choices, MonarchAction.RAISE_TAX_ACT));
        assertTrue(choicesContain(choices, MonarchAction.LOWER_TAX_WAR));
        assertTrue(choicesContain(choices, MonarchAction.LOWER_TAX_OTHER));

        dutch.changePlayerType(Player.PlayerType.REBEL);
        choices = dutch.getMonarch().getActionChoices();
        assertTrue(choices.isEmpty());

    }

    private boolean choicesContain(List<RandomChoice<MonarchAction>> choices, MonarchAction action) {
        for (RandomChoice<MonarchAction> choice : choices) {
            if (choice.getObject() == action) {
                return true;
            }
        }
        return false;
    }
    
    // Added by me
    public void testGetSupportSea() {
    	Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Initially, supportSea should be false
        assertFalse("The initial supportSea should be false", monarch.getSupportSea());

        // Set the supportSea field to true and test
        monarch.setSupportSea(true);
        assertTrue("The supportSea should be true", monarch.getSupportSea());

        // Set the supportSea field to false and test
        monarch.setSupportSea(false);
        assertFalse("The supportSea should be false", monarch.getSupportSea());
    }
    
    //Added by me
    public void testUpdateInterventionForce() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Simulate setting up the intervention force with a known unit type
        Specification spec = game.getSpecification();
        AbstractUnit soldierUnit = new AbstractUnit(spec.getUnitType("model.unit.freeColonist"), "model.role.soldier", 1);
        Force interventionForce = monarch.getInterventionForce();
        interventionForce.getLandUnits().add(soldierUnit);

        // Set the game turn to a value that will trigger updates
        game.setTurn(new Turn(20));

        // Call the method under test
        monarch.updateInterventionForce();

        // Verify the intervention force has been updated
        for (AbstractUnit unit : interventionForce.getLandUnits()) {
            assertTrue("Unit number should be greater than 1", unit.getNumber() > 1);
        }

        // Verify the naval units are added if needed
        if (interventionForce.getSpaceRequired() > interventionForce.getCapacity()) {
            assertFalse("Naval units should be added", interventionForce.getNavalUnits().isEmpty());
        }
    }
    
    //Added by me
    public void testGetExpeditionaryForce() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Ensure the expeditionary force is correctly initialized
        Force expeditionaryForce = monarch.getExpeditionaryForce();
        assertNotNull("The expeditionary force should not be null", expeditionaryForce);
    }

    
    //Added by me
    public void testGetInterventionForce() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Ensure the intervention force is correctly initialized
        Force interventionForce = monarch.getInterventionForce();
        assertNotNull("The intervention force should not be null", interventionForce);
    }
    
    //Added by me
    public void testGetMercenaryForce() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Ensure the mercenary force is correctly initialized
        Force mercenaryForce = monarch.getMercenaryForce();
        assertNotNull("The mercenary force should not be null", mercenaryForce);
    }
    
    //Added by me
    public void testSetAndGetDispleasure() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Initially, displeasure should be false
        assertFalse("The initial displeasure should be false", monarch.getDispleasure());

        // Set the displeasure field to true and test
        monarch.setDispleasure(true);
        assertTrue("The displeasure should be true", monarch.getDispleasure());

        // Set the displeasure field to false and test
        monarch.setDispleasure(false);
        assertFalse("The displeasure should be false", monarch.getDispleasure());
    }
    
    //Added by me
    public void testRaiseTax() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        Random random = new Random();
        int initialTax = dutch.getTax();

        // Raise the tax and check that it has increased
        int newTax = monarch.raiseTax(random);
        assertTrue("The new tax should be greater than the initial tax", newTax > initialTax);
    }
    
    public void testGetWarSupportForce() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);

        // Ensure the war support force is correctly initialized
        Force warSupportForce = monarch.getWarSupportForce();
        assertNotNull("The war support force should not be null", warSupportForce);
    }

    public void testCalculateStrength() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);
        Force force = monarch.getExpeditionaryForce();

        // Ensure the calculate strength method returns a value
        double strength = force.calculateStrength(false);
        assertTrue("The calculated strength should be non-negative", strength >= 0);
    }


    public void testGetSupport() {
        Game game = getStandardGame();
        game.setMap(getTestMap());
        Player dutch = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, dutch);
        Random random = new Random();

        // Get naval support
        List<AbstractUnit> navalSupport = monarch.getSupport(random, true);
        assertNotNull("The naval support should not be null", navalSupport);
        assertFalse("The naval support should not be empty", navalSupport.isEmpty());

        // Get land support
        List<AbstractUnit> landSupport = monarch.getSupport(random, false);
        assertNotNull("The land support should not be null", landSupport);
        assertFalse("The land support should not be empty", landSupport.isEmpty());
    }
    
    ////////////////////
    
    public void testLowerTaxAction() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        player.setTax(Monarch.MINIMUM_TAX_RATE + 20); // Set tax above the minimum.
        assertTrue("Lower tax action should be valid", monarch.actionIsValid(Monarch.MonarchAction.LOWER_TAX_OTHER));
        int loweredTax = monarch.lowerTax(new Random());
        assertTrue("Lowered tax should be less than current tax", loweredTax < player.getTax());
    }

    public void testNoActionValidity() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        assertTrue("No action should always be valid", monarch.actionIsValid(Monarch.MonarchAction.NO_ACTION));
    }
    public void testWaiveTaxAction() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        player.setTax(50);
        monarch.actionIsValid(Monarch.MonarchAction.WAIVE_TAX);
        monarch.raiseTax(new Random()); // Simulate tax raise to enable tax waiver
        assertTrue("Waive tax should be valid after tax raise", monarch.actionIsValid(Monarch.MonarchAction.WAIVE_TAX));
    }

    public void testDeclareWar() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        assertFalse("Declare war should not be valid if there are no enemies", monarch.actionIsValid(Monarch.MonarchAction.DECLARE_WAR));
        // Add a mock enemy to enable war declaration
        Player enemy = new Player(game, "model.nation.french");
        game.addPlayer(enemy);
        assertFalse("Declare war should be valid with available enemy", monarch.actionIsValid(Monarch.MonarchAction.DECLARE_WAR));
    }

    public void testAddToREF() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        assertTrue("Add to REF should not be valid when REF is full", monarch.actionIsValid(Monarch.MonarchAction.ADD_TO_REF));
        // Simulate conditions to add to REF
        Force ref = monarch.getExpeditionaryForce();
        AbstractUnit newUnit = new AbstractUnit(game.getSpecification().getUnitType("model.unit.freeColonist"), "model.role.soldier", 50);
        ref.add(newUnit);
        assertTrue("Add to REF should be valid now", monarch.actionIsValid(Monarch.MonarchAction.ADD_TO_REF));
    }

    public void testSupportLand() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        
        assertFalse("Support land should be valid during war", monarch.actionIsValid(Monarch.MonarchAction.SUPPORT_LAND));
    }
    
    public void testDispleasureEffect() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        assertFalse("Displeasure should be false initially", monarch.getDispleasure());
        monarch.setDispleasure(true);
        assertTrue("Displeasure should be true after set", monarch.getDispleasure());
    }

    public void testUpdateREFUnits() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        Force ref = monarch.getExpeditionaryForce();
        int originalCount = ref.getLandUnits().size();
        AbstractUnit newUnit = new AbstractUnit(spec().getUnitType("model.unit.freeColonist"), "model.role.soldier", 20);
        ref.add(newUnit);
        int newCount = ref.getLandUnits().size();
        assertTrue("REF units should increase", newCount > originalCount);
    }
    
//    public void testCalculateNavalStrength() {
//        Game game = getStandardGame();
//        Player player = game.getPlayerByNationId("model.nation.dutch");
//        Monarch monarch = new Monarch(game, player);
//        Force navy = new Force();
//        AbstractUnit ship = new AbstractUnit(game.getSpecification().getUnitType("model.unit.manOfWar"), "model.role.default", 1);
//        navy.add(ship);
//        double strength = navy.calculateStrength(true);
//        assertTrue("Naval strength should be greater than zero for a ship", strength > 0);
//    }

    public void testLowerTaxWithoutAbility() {
        Game game = getStandardGame();
        Player player = game.getPlayerByNationId("model.nation.dutch");
        Monarch monarch = new Monarch(game, player);
        player.setTax(Monarch.MINIMUM_TAX_RATE); // Set tax to minimum possible.
        assertFalse("Lower tax action should be invalid when tax is minimum", monarch.actionIsValid(Monarch.MonarchAction.LOWER_TAX_OTHER));
    }



}

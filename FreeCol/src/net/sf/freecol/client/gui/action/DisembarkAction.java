package net.sf.freecol.client.gui.action;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import net.sf.freecol.client.FreeColClient;
import net.sf.freecol.client.gui.ChoiceItem;
import net.sf.freecol.common.i18n.Messages;
import net.sf.freecol.common.model.Direction;
import net.sf.freecol.common.model.StringTemplate;
import net.sf.freecol.common.model.Tile;
import net.sf.freecol.common.model.Unit;
import net.sf.freecol.common.model.Unit.UnitState;

public class DisembarkAction extends UnitAction {

	public static final String id = "disembarkAction";
	//need to figure out land proximity. 
	Direction direction;
	
    /**
     * Creates this action.
     *
     * @param freeColClient The <code>FreeColClient</code> for the game.
     */
    public DisembarkAction(FreeColClient freeColClient) {
        super(freeColClient, id);
        //Need to put a different image here
        addImageIcons("build");
    }
    
 // Override FreeColAction

    /**
     * {@inheritDoc}
     */
    @Override
    
    //need to figure out the logic for when it should be enabled
    protected boolean shouldBeEnabled() {
    	//What we need is the ability to check for land on one (or more) sides of the boat.
    	//This would then be used to set the direction that would be passed into the moveDisembark function.
    	//Unfortunately COVID slowed me down and I was not able to finish this last part of the implementation 
    	//-Katelyn
        return true;
        
    }
    


    // Interface ActionListener

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
    	igc().moveDisembark(getGUI().getActiveUnit(), direction);
    }
	
	

}